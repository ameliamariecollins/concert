// Ensure namespace root
CONCERT = window.CONCERT || {};

(function($){ 'use strict';
  //-- Check for dependencies
  // (jquery ensured by outer function)
  // jquery-ui
  if (!$.ui) return;

  // Create slot in namespace
  CONCERT.Certificate = CONCERT.Certificate || function() {
    var that = this;

    // Instance storage
    that.editables = [
      ".certTitle",
      ".certName",
      ".certStatement",
      ".certDescription",
      ".certDateComment"
    ];

    $(document).ready(function() {
      $.each(that.editables, function(id, name) {
        $(name).attr('contenteditable','true');
      });
    }); // document ready

  }; // object
})(jQuery); // jQuery parameter wrapper

// vim:sw=2:et
